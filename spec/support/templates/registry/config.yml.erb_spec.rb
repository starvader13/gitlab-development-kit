# frozen_string_literal: true

RSpec.describe 'support/templates/registry/config.yml.erb' do
  let(:registry_settings) { {} }
  let(:yaml) { { 'registry' => registry_settings } }
  let(:source) { |example| example.example_group.top_level_description }
  let(:expected_result) do
    {
      'auth' => {
        'token' => {
          'autoredirect' => false,
          'issuer' => 'gitlab-issuer',
          'realm' => 'http://127.0.0.1:3000/jwt/auth',
          'rootcertbundle' => '/etc/docker/registry/localhost.crt',
          'service' => 'container_registry'
        }
      },
      'health' => { 'storagedriver' => { 'enabled' => true, 'interval' => '10s', 'threshold' => 3 } },
      'http' => { 'addr' => :'5000', 'headers' => { 'X-Content-Type-Options' => ['nosniff'] } },
      'log' => { 'level' => 'info' },
      'storage' => {
        'cache' => { 'blobdescriptor' => 'inmemory' },
        'delete' => { 'enabled' => true },
        'filesystem' => { 'rootdirectory' => '/var/lib/registry' },
        'maintenance' => {
          'uploadpurging' => { 'age' => '8h', 'dryrun' => false, 'enabled' => true, 'interval' => '1h' }
        }
      },
      'validation' => { 'disabled' => true },
      'version' => 0.1
    }
  end

  before do
    config = GDK::Config.new(yaml: yaml)
    allow(GDK).to receive(:config).and_return(config)
  end

  subject(:output) do
    renderer = GDK::Templates::ErbRenderer.new(source)
    YAML.safe_load(renderer.render_to_string, permitted_classes: [Symbol])
  end

  context 'with defaults' do
    it { expect(output).to eq(expected_result) }
  end

  context 'with notifications_enabled' do
    let(:registry_settings) { { 'notifications_enabled' => true } }
    let(:expected_result) do
      super().merge(
        'notifications' => {
          'endpoints' => [
            {
              'name' => 'gitlab-rails',
              'url' => 'http://127.0.0.1:3000/api/v4/container_registry_event/events',
              'headers' => {
                'Authorization' => ['notifications_secret']
              },
              'timeout' => '500ms',
              'threshold' => 5,
              'backoff' => '1s'
            }
          ]
        }
      )
    end

    it { expect(output).to eq(expected_result) }
  end
end
